package com.bgiaa.o_eat_arduino_androidmobileapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.bgiaa.o_eat_arduino_androidmobileapp.Models.Glaciere;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class GlaciereInfoActivity extends AppCompatActivity {

    private String serial_no;
    private int temperature;
    private double coordX;
    private double coordY;

    private Glaciere glaciere;

    private TextView uuidTextView;
    private TextView temperatureTextView;
    private TextView coordXTextView;
    private TextView coordYTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glaciere_info);

        uuidTextView = findViewById(R.id.serial_no_TextView);
        temperatureTextView = findViewById(R.id.temperature_TextView);
        coordXTextView = findViewById(R.id.coord_x_TextView);
        coordYTextView = findViewById(R.id.coord_y_TextView);

        glaciere = getIntent().getParcelableExtra("glaciere");

        //getJson();
        serial_no = glaciere.getSerial_no();
        temperature = glaciere.getTemperature();
        coordX = glaciere.getCoordX();
        coordY = glaciere.getCoordY();

        uuidTextView.setText(serial_no);
        temperatureTextView.setText(temperature + "");
        coordXTextView.setText(coordX + "");
        coordYTextView.setText(coordY + "");
    }

    public void getJson(){
        String json;

        try{
            InputStream jsonInputStream = getAssets().open("glaciere.json");
            int size = jsonInputStream.available();
            byte[] buffer = new byte[size];
            jsonInputStream.read(buffer);
            jsonInputStream.close();

            json = new String(buffer, "UTF-8");
            JSONArray jsonArray = new JSONArray(json);

            for(int i = 0; i < jsonArray.length(); i++){
                JSONObject obj = jsonArray.getJSONObject(i);
                JSONObject coordonnees = obj.getJSONObject("coord");
                serial_no = obj.getString("_uuid");
                temperature = obj.getInt("temp");
                coordX = coordonnees.getDouble("x");
                coordY = coordonnees.getDouble("y");
            }
        }catch(IOException e) {
            e.printStackTrace();
        }catch(JSONException e){
            e.printStackTrace();
        }
    }
}
