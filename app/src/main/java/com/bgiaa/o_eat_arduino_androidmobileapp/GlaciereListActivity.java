package com.bgiaa.o_eat_arduino_androidmobileapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bgiaa.o_eat_arduino_androidmobileapp.Adapters.GlaciereAdapter;
import com.bgiaa.o_eat_arduino_androidmobileapp.HttpRequest.LoginErrorMessageOEatAPI;
import com.bgiaa.o_eat_arduino_androidmobileapp.Models.Glaciere;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GlaciereListActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    String apiUrl;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glaciere_list);

        sharedPreferences = getApplicationContext().getSharedPreferences("OEatAppSharedPreferences", MODE_PRIVATE);

        apiUrl = getString(R.string.api_url);
        recyclerView = findViewById(R.id.glacieresRecyclerView);

        displayGlacieresListFAN();
    }

    private void displayGlacieresListFAN() {
        JSONObject postRequestBody = new JSONObject();

        try {
            //postRequestBody.put("query", "wrongQuery");
            //postRequestBody.put("noQuery", "emptyQuery");
            postRequestBody.put("query", "{resource__oeat__glacieres(_page:1,_size:10,_logop:\"AND\"){items{closed,dev_id,name,coord{x,y},temp,_uuid}}}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(apiUrl + "api/graphql")
                .addHeaders("x-token", sharedPreferences.getString("token", null))
                .addHeaders("x-refresh-token", sharedPreferences.getString("refresh", null))
                //.addHeaders("x-token", UserLoginActivity.sessionToken)
                //.addHeaders("x-refresh-token", UserLoginActivity.sessionRefresh)
                .addJSONObjectBody(postRequestBody) // posting json
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject data = response.getJSONObject("data");
                            JSONObject resourceOeatGlacieres = data.getJSONObject("resource__oeat__glacieres");
                            JSONArray items = resourceOeatGlacieres.getJSONArray("items");

                            ArrayList<Glaciere> glacieresList = new ArrayList<Glaciere>();

                            for(int i = 0; i < items.length(); i++){
                                JSONObject obj = items.getJSONObject(i);
                                JSONObject coordonnees = obj.getJSONObject("coord");
                                glacieresList.add(new Glaciere(obj.getString("_uuid"), obj.getInt("temp"), coordonnees.getDouble("x"), coordonnees.getDouble("y")));
                            }

                            GlaciereAdapter glaciereAdapter = new GlaciereAdapter(GlaciereListActivity.this, glacieresList);
                            recyclerView.setAdapter(glaciereAdapter);
                            recyclerView.setLayoutManager(new LinearLayoutManager(GlaciereListActivity.this));

                        } catch (JSONException e) {
                            toastErrorMessage(e.getMessage());
                            e.printStackTrace();

                            try {
                                JSONArray errors = response.getJSONArray("errors");

                                for(int i = 0; i < errors.length(); i++){
                                    JSONObject error = errors.getJSONObject(i);
                                    String message = error.getString("message");
                                    //JSONArray locations = error.getJSONArray("locations");
                                    toastErrorMessage(message);
                                }
                            } catch (JSONException ex) {
                                toastErrorMessage(ex.getMessage());
                                ex.printStackTrace();
                            }

                            Intent intent = new Intent(GlaciereListActivity.this, UserLoginActivity.class);
                            startActivity(intent);
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        LoginErrorMessageOEatAPI apiError;
                        LoginErrorMessageOEatAPI.MessageClass message;
                        String status;
                        String Message;

                        if (error.getErrorCode() != 0) {
                            if (error.getErrorCode() == 400){
                                apiError = error.getErrorAsObject(LoginErrorMessageOEatAPI.class);
                                message = apiError.message;
                                status = apiError.status;
                                Message = message.Message;
                                //LoginErrorMessageOEatAPI.MessageClass.FieldMessagesClass FieldMessages = message.FieldMessages;
                                //String password = FieldMessages.Password;
                                toastErrorMessage(status + ": " + Message);
                            }
                            else{
                                apiError = error.getErrorAsObject(LoginErrorMessageOEatAPI.class);
                                message = apiError.message;
                                status = apiError.status;
                                Message = message.Message;
                                toastErrorMessage(status + ": " + Message);
                                //toastErrorMessage("errorCode : " + error.getErrorCode() + "\n\n" + "errorBody : " + error.getErrorBody() + "\n\n" + "errorDetail : " + error.getErrorDetail());
                            }

                        } else {
                            toastErrorMessage(error.getErrorDetail()); // : connectionError, parseError, requestCancelledError
                        }

                        Intent intent = new Intent(GlaciereListActivity.this, UserLoginActivity.class);
                        startActivity(intent);
                    }
                });
    }

    private void toastErrorMessage(String message){
        Toast toastTest = Toast.makeText(GlaciereListActivity.this, message, Toast.LENGTH_LONG);
        toastTest.setGravity(Gravity.BOTTOM, 0, 200);
        toastTest.show();
    }
}
